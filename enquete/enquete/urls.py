
from django.contrib import admin
from django.urls import path, include

from main import views
from django.views.generic import RedirectView


urlpatterns = [
    path('', include('main.urls')),
    path('agenda/', include('agenda.urls')),
    path('admin/', admin.site.urls),
]
