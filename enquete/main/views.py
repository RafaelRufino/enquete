from django.shortcuts import render, get_object_or_404
from django.http import  HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import Pergunta, Opcao, Categoria


# Create your views here.

def home(request):
   # categorias = 'teste'
    categorias = Categoria.objects.all()
    pergunta_list = Pergunta.objects.all()
    return render(request, 'main/pergunta_list.html', {'categorias': categorias, 'pergunta_list':pergunta_list})

# alteração aqui
def root(request):
    return render(request, 'main/root.html', {})

#enqueteView
class IndexView(generic.ListView):

    def get_queryset(self):
        return Pergunta.objects.filter(
            data_publicacao__lte=timezone.now()
            ).order_by('-data_publicacao')[:5]

#enqueteView
# class Enquete2View(generic.ListView):

#     def get_queryset(self):
#         return Pergunta.objects.filter(
#             data_publicacao__lte=timezone.now()
#             ).order_by('-data_publicacao')[:5]
# ate aqui




class DetalhesView(generic.DetailView):
    model = Pergunta
    def get_queryset(self):
        return Pergunta.objects.filter(data_publicacao__lte = timezone.now())


class ResultadoView(generic.DetailView):
    model = Pergunta
    template_name = 'main/resultado.html'



def votacao(request, id_enquete):
    pergunta = get_object_or_404(Pergunta, pk=id_enquete)
    try:
        opcao_selecionada = pergunta.opcao_set.get(pk=request.POST['opcao'])
    except (KeyError, Opcao.DoesNotExist):
        return render(request, 'main/detalhes.html', {
            'pergunta': pergunta,
            'error_message': "Selecione uma opcão VÁLIDA!"
            })
    else:
        opcao_selecionada.votos += 1
        opcao_selecionada.save()
        return HttpResponseRedirect(
            reverse('main:resultado', args=(pergunta.id,))
            )

