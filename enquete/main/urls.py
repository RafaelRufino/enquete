from django.urls import path
from . import views
from django.views.generic import RedirectView
from main import views
app_name = 'main'
urlpatterns = [

    path('', views.root, name='root'),
    
    # path('', RedirectView.as_view(url='tarefas/todos/')),

    path('enquete',
        views.IndexView.as_view(), name='index'
        ),
 


    path('enquete/<int:pk>',
        views.DetalhesView.as_view(), name ='detalhes'
    ),
    path('enquete/<int:pk>/resultado',
        views.ResultadoView.as_view(), name ='resultado'
    ),
    path('enquete/<int:id_enquete>/votacao',
        views.votacao, name ='votacao'
    ),
]


