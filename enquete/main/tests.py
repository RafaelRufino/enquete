#from django.test import TestCase

# Create your tests here.
import datetime
from django.utils import timezone
from django.test import TestCase
from django.urls import reverse
from .models import Pergunta


# Create your tests here.
class PerguntaTeste(TestCase):
    def test_publicada_recentemente_com_pergunta_no_futuro(self):
        """
        o métado publicada_recentemente precisa retornar FALSE quando se
        tratar deperguntas com dat de publicacao no futuro
        """


        data = timezone.now() + datetime.timedelta(seconds=1)
        pergunta_futura = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_futura.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_anterior_a_24h_no_passado(self):
        """
        o metado publicada_recentemente DEVE retonar False quando se tratar
        de uma data de publicação anterior a 24hs no passado.
        """
        data = timezone.now() - datetime.timedelta(days=1, seconds=1)
        pergunta_passada = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_passada.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_nas_ultimas_24hs(self):

        """
        o metado publicada_recentemente DEVE retornar TRUE quando se tratar de
        uma data de publicação dentro das ultimas 24hrs.

        """
        data = timezone.now() - datetime.timedelta(hours =23, minutes = 59, seconds=59)
        pergunta_ok = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_ok.publicada_recentemente(), True)

def criar_pergunta(texto, dias):

    """
    função para criação de uma pergunta para texto e uma variação de dias

    """

    data = timezone.now() + datetime.timedelta(days=dias)
    return Pergunta.objects.create(texto=texto, data_publicacao=data)

class IndexViewTeste(TestCase):
    def test_sem_perguntas_cadastradas(self):
        """
        Exibe menssagem especifica quando não houverem perguntas cadastradas.
        """
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Não há enquetes cadastrados até o nomento!")
        self.assertQuerysetEqual(resposta.context['pergunta_list'], [])


    def test_com_pergunta_no_passado(self):
        """
        Exibe normalmente pergunta no passado.
        """
        criar_pergunta(texto='Pergunta no passado', dias=-30)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertQuerysetEqual(resposta.context['pergunta_list'], ['<Pergunta: Pergunta no passado>'])

    def test_com_pergunta_no_futuro(self):
        """
        Perguntas com data de publicação no futuro Não DEVEM ser exibidas.
        """

        criar_pergunta(texto = "Pergunta no futuro", dias = 1)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Não há enquetes cadastrados até o nomento!")
        self.assertQuerysetEqual(resposta.context['pergunta_list'], [])

    def test_pergunta_no_passado_e_no_futuro(self):
        """
        Pergunta com data de publicação no passado são exibidas e com a data de publicação no futuro são omitidas.
        """
        criar_pergunta(texto = "Pergunta no passado ", dias =-1)
        criar_pergunta(texto = "Pergunta no futuro", dias = 1)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)

        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(
            resposta.context['pergunta_list'],['<Pergunta: Pergunta no passado >']
        )


    def test_duas_perguntas_no_passado(self):

        """
        Exibe normalmente mais de uma Pergunta com data de publicação no passado.

        """
        criar_pergunta(texto = "Pergunta no passado 1", dias =-1)
        criar_pergunta(texto = "Pergunta no passado 2", dias =-5)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)

        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(resposta.context['pergunta_list'],
        ['<Pergunta: Pergunta no passado 1>',
        '<Pergunta: Pergunta no passado 2>']
        )

class DetalhesViewTeste(TestCase):
    def test_pergunta_no_futuro(self):
        """
        Deverá retornar um erro 404 ao indicar uma pergunta com dta no futuro.

        """

        pergunta_futuro = criar_pergunta(texto = "Pergunta no futuro", dias=5)
        resposta = self.client.get(
            reverse('main:detalhes', args=[pergunta_futuro.id,])

        )

        self.assertEqual(resposta.status_code, 404)


    def test_pergunta_no_passado(self):

        """
        Devera exibir normalmente uma pergunta com data no passado.

        """

        pergunta_passado = criar_pergunta(texto = "Pergunta no passado", dias=-1)
        resposta = self.client.get(
            reverse('main:detalhes', args=[pergunta_passado.id,])

        )
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, pergunta_passado.texto)















