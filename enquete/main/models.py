from django.db import models
from django.utils import timezone
import datetime

class Usuario(models.Model):
  login = models.CharField(max_length = 50)
  senha = models.CharField(max_length = 50)
  email = models.CharField(max_length = 100)
  def __str__(self):
      return self.login

class Categoria(models.Model):
    titulo = models.CharField(max_length = 200)
    descricao = models.TextField()
    data_de_criacao = models.DateTimeField('Data de Criação')
    def __str__(self):
        return self.titulo

class Pergunta(models.Model):
    texto = models.CharField(max_length = 200)
    data_publicacao = models.DateTimeField('Data de Publicação')
    categoria = models.ForeignKey(
        Categoria,
        on_delete=models.CASCADE,
        default = None,
        null =True
    )
    usuario = models.ForeignKey(
        Usuario,
        on_delete=models.CASCADE,
        default = None,
        null =True
    )
    def __str__(self):
        return self.texto
    def publicada_recentemente(self):
        agora = timezone.now()
        return  agora-datetime.timedelta(days=1) <= self.data_publicacao <= agora
    publicada_recentemente.admin_order_field = 'data_publicacao'
    publicada_recentemente.boolean = True
    publicada_recentemente.short_description = 'ultimas 24hs?'

class Opcao(models.Model):
    texto = models.CharField(max_length = 200)
    votos = models.IntegerField(default = 0)
    pergunta= models.ForeignKey(Pergunta, on_delete=models.CASCADE)
    def __str__(self):
        return self.texto

