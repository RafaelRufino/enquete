from django.contrib import admin
from .models import Pergunta, Opcao,Categoria, Usuario

# Ordernar campos.
class OpcaoInline(admin.TabularInline):
    model = Opcao
    extra = 1

class PerguntaAdmin(admin.ModelAdmin):
    # fields =
    fieldsets = [

        ('Informação de usuario', {'fields':  ['usuario']}),
        (None, {'fields': ['texto']}),
        ('informações de data', {'fields': ['data_publicacao']}),


    ]
    inlines = [OpcaoInline]
    list_display = ('texto','id', 'data_publicacao', 'publicada_recentemente')
    list_filter = ['data_publicacao']
    #pesquisar por texto
    search_fields = ['texto']

admin.site.register(Pergunta, PerguntaAdmin)
# admin.site.register(Opcao)
admin.site.register(Categoria)
admin.site.register(Usuario)
