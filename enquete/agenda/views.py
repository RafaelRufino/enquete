from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login,logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .models import Tarefa
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required

# Create your views here.


def index(request):
	
		return render(request, 'agenda/list.html', {})


@login_required(login_url='/login/')
def list_todos_tarefas(request):
	tarefa = Tarefa.objects.all()
	return render(request, 'agenda/list.html', {'tarefa':tarefa})

@login_required(login_url='/login/')
def list_user_tarefas(request):
	tarefa = Tarefa.objects.filter(user=request.user)
	return render(request, 'agenda/list.html', {'tarefa':tarefa})

@login_required(login_url='/login/')
def tarefa_detalhes(request, id):
	tarefa = Tarefa.objects.get(id=id)
	return render(request, 'agenda/tarefa.html', {'tarefa':tarefa})	

@login_required(login_url='/login/')
def cadastrar_tarefa(request):
	tarefa_id = request.GET.get('id')
	if tarefa_id:
		tarefa = Tarefa.objects.get(id=tarefa_id)
		if tarefa.user == request.user:
			return render(request, 'agenda/cadastrar_tarefa.html', {'tarefa':tarefa})
	return render(request, 'agenda/cadastrar_tarefa.html')

@login_required(login_url='/login/')
def set_tarefa(request):
	atividade = request.POST.get('atividade')
	statusTarefa = request.POST.get('statusTarefa')
	diaSemana = request.POST.get('diaSemana')
	disciplina = request.POST.get('disciplina')
	tarefa_id = request.POST.get('tarefa_id')
	user = request.user
	if tarefa_id:
		tarefa = Tarefa.objects.get(id=tarefa_id)
		if user == tarefa.user:
			tarefa.atividade = atividade
			tarefa.disciplina = disciplina
			tarefa.diaSemana = diaSemana
			tarefa.statusTarefa = statusTarefa
			tarefa.save()
	else:
		tarefa = Tarefa.objects.create(atividade=atividade, disciplina=disciplina, diaSemana=diaSemana,statusTarefa=statusTarefa, user=user)
	url = '/agenda/tarefas/todos/'.format(tarefa.id)
	return redirect (url)	


@login_required(login_url='/login/')	

def delete_tarefa(request, id):
	tarefa = Tarefa.objects.get(id=id) 
	if tarefa.user == request.user:
		tarefa.delete()
	return redirect('/agenda/tarefas/todos/')


def cadastrar_usuario(request):
	if request.method == 'POST':	
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/'),messages.error(request, 'Usuário/Senha inválidos. Favor tentar novamente.')

	else:
		form = UserCreationForm()
	return render(request,'/agenda/cadastrar_usuario.html',{'form':form})

def login_usuario(request):
	if request.method == 'POST':	
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/')
	else:
		form = UserCreationForm()
	return render(request,'agenda/login.html',{'form':form})



def logout_usuario(request):
	logout(request)
	return redirect('/agenda/login/')


def submit(request):
	if request.POST:
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username=username, password=password)
		if user is not None:
			login(request, user)
			return redirect('/')
		else:
			messages.error(request, 'Usuário/Senha inválidos. Favor tentar novamente.')
	return redirect('/login/')
	