from django.contrib import admin
from django.urls import path
from . import views
from agenda import views

from django.views.generic import RedirectView
app_name = 'agenda'
urlpatterns = [
    
    path('', views.index, name='index'),
    


    path('admin/', admin.site.urls),
    path('tarefas/todos/',views.list_todos_tarefas),


    path('tarefas/usuario/', views.list_user_tarefas),
    path('tarefas/detalhes/<slug:id>/', views.tarefa_detalhes),
    path('tarefas/registro/', views.cadastrar_tarefa),
    path('tarefas/registro/submit', views.set_tarefa),
    path('tarefas/delete/<slug:id>/', views.delete_tarefa),

    path('login/',views.login_usuario),
    
    path('login/registro',views.cadastrar_usuario),
    path('login/submit',views.submit),
    path('logout/',views.logout_usuario),

    path('submit', RedirectView.as_view(url='tarefas/todos/'))



]
