from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
class Tarefa(models.Model):
	atividade  = models.CharField(max_length = 60)
	disciplina = models.CharField(max_length = 60)
	statusTarefa = models.CharField(max_length = 60)
	
	diaSemana = models.DateTimeField("Data de inicio", null = True)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	


	def __str__(self):
		return  "Tarefa #%d: %s" %(self.id, self.atividade)

	def data_publicada(self):
        	return datetime.datetime.today().year - self.diaSemana.year



